# daverona/compose/gitlab

## Prerequisites

* `gitlab.example` mapped to `127.0.0.1` &mdash; check [this](https://gitlab.com/daverona/compose/dnsmasq) out
* [Traefik](https://docs.traefik.io/) &mdash; check [this](http://gitlab.com/daverona/compose/traefik) out

## Quick Start

```bash
cp .env.example .env
# edit .env
docker-compose up --detach
```

Wait until gitlab is up (it will take a while) and visit [http://gitlab.example](http://gitlab.example).
Set the password for `root` and log in.

## References

* GitLab CE Installation: [https://docs.gitlab.com/omnibus/docker/](https://docs.gitlab.com/omnibus/docker/)
* Configuring Omnibus GitLab: [https://docs.gitlab.com/omnibus/settings/README.html](https://docs.gitlab.com/omnibus/settings/README.html)
* GitLab repository: [https://gitlab.com/gitlab-org/gitlab](https://gitlab.com/gitlab-org/gitlab)
* GitLab CE registry: [https://hub.docker.com/r/gitlab/gitlab-ce](https://hub.docker.com/r/gitlab/gitlab-ce)
